import http from "@/services/http";

const api = {
  getProfile: () => {
    return http.get("/profile/");
  },
  getSkills: () => {
    return http.get("/skills/");
  },
  getEducation: () => {
    return http.get("/history/education");
  },
  getWorking: () => {
    return http.get("/history/working");
  }
};

export default api;
