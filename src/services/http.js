import axios from "axios";

const instance = axios.create({
  baseURL: "https://api.appelius-theo.fr"
});

instance.defaults.headers.patch["Content-Type"] = "application/json";
instance.defaults.headers.put["Content-Type"] = "application/json";
instance.defaults.headers.post["Content-Type"] = "application/json";

/**
 * @param {Object.<string, any>} req
 * @returns {Promise<Array|Object<string, any>} Promise<Array|Object<string, any>
 */
function requestTemplate(req) {
  return new Promise((resolve, reject) => {
    instance(req)
      .then(response => {
        try {
          resolve(response.data.data);
        } catch {
          reject("une erreur est survenue.");
        }
      })
      .catch(error => {
        reject(getErrors(error));
      });
  });
}

/**
 * @param {string} error
 * @returns {string} string
 */
function getErrors(error) {
  const errorMessage = error.response
    ? error.response.data.message
    : "une erreur est survenue.";
  try {
    return JSON.parse(errorMessage);
  } catch (e) {
    return errorMessage;
  }
}

const http = {
  /**
   * @param {string} url
   * @param {Object.<string, any>} params
   * @returns {Promise<Array|Object<string, any>} Promise<Array|Object<string, any>
   */
  get: (url, params = {}) => {
    return new Promise((resolve, reject) => {
      const req = {
        method: "get",
        url
      };

      if (Object.keys(params).length > 0) {
        const urlParams = new URLSearchParams();

        Object.entries(params).forEach(elem => {
          urlParams.append(elem[0], elem[1]);
        });

        req.params = urlParams;
      }

      requestTemplate(req)
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

export default http;
